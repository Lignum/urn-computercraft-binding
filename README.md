# About this Project
This is a direct mapping of the ComputerCraft API from Lua to Urn.

## Example
```common_lisp
(import cc/term term)
(import cc/colours colours)

(term/setBackgroundColour colours/red)
(term/clear)
(term/setCursorPos 1 1)
(term/write "Hello Urn!")
```

All other APIs can be imported by their exact name with the cc prefix, just like this.

## Installation
Copy the `cc/` directory inside `lib/` into one of Urn's library search directories; by default this includes `urn/` and `urn/lib`, where `urn/` is the root directory of your Urn installation.
Alternatively, you can specify your own directory by passing `--include` to `run.lua`.